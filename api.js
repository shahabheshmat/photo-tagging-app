
const inputFile = document.getElementById('input-file');
const previewContainer = document.getElementById('previewer-div');
const previewImage = previewContainer.querySelector('.image-preview__image');
const previewDefaultText = previewContainer.querySelector('.image-preview-default-text');
const apiKey = 'acc_0b4c71d40e23166';
const apiSecret = '34a67e3f12be8613a82f079ad0a5be4d';
let uploadIdResult;
let faceCoordinationResult;

inputFile.addEventListener('change', () => {
    const file = inputFile.files[0];

    if (file) {
        const reader = new FileReader();
        previewDefaultText.style.display = 'none';
        previewImage.style.display = 'block';

        reader.addEventListener('load', () => {
            previewImage.setAttribute('src', reader.result);
        })
        reader.readAsDataURL(file);

        uploadImage()
    }
})

function uploadImage() {
    const formData = new FormData();

    formData.append('image', inputFile.files[0]);
    fetch('https://api.imagga.com/v2/uploads', {
        method: 'POST',
        headers: {
            'Authorization': 'Basic YWNjXzBiNGM3MWQ0MGUyMzE2NjozNGE2N2UzZjEyYmU4NjEzYTgyZjA3OWFkMGE1YmU0ZA=='
        },
        body: formData
    })
        .then(response => response.json())
        .then(result => {
            uploadIdResult = result.result.upload_id
            getFaceDetection();
        })
        .catch(error => {
            console.error('Error:', error);
        });
}


function getFaceDetection() {
    const uploadedURL = 'https://api.imagga.com/v2/faces/detections?image_upload_id=' + uploadIdResult;

    fetch(uploadedURL, {
        method: 'GET',
        headers: {
            'Authorization': 'Basic YWNjXzBiNGM3MWQ0MGUyMzE2NjozNGE2N2UzZjEyYmU4NjEzYTgyZjA3OWFkMGE1YmU0ZA=='
        }
    })
        .then(response => response.json())
        .then(result => {
            console.log('Success:', result);
            faceCoordinationResult = result.result.faces;

            for (let i = 0; i < faceCoordinationResult.length; i++) {
                console.log(faceCoordinationResult[i].coordinates.height);
                console.log(faceCoordinationResult[i].coordinates.width);
                console.log(faceCoordinationResult[i].coordinates.xmax);
                console.log(faceCoordinationResult[i].coordinates.xmin);
                console.log(faceCoordinationResult[i].coordinates.ymax);
                console.log(faceCoordinationResult[i].coordinates.ymin);
            }

        })
        .catch(error => {
            console.error('Error:', error);
        });
}